package challenge;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class InputManager {
	
	
	private static String file;
	private static int i;
	private static int j;
	private static FileReader fir;
	private static BufferedReader reader;
   
	
	public static void initialize(String filename) {
		
		file = filename;
		try {
			fir = new FileReader(file);
			reader = new BufferedReader(fir);	
			
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		
		String[] temp = convertLine(readLine());
		i = Integer.parseInt(temp[0]);
		j = Integer.parseInt(temp[1]);
			
		
	}
	
	public static int[][] create(int[][] map) {
		for (int x = 0;x<i;x++) {
			String[] temp = convertLine(readLine());
			for (int y = 0;y < j;y++) {
				
				map[x][y] = Integer.parseInt(temp[y]);
			}
		}
		
		
		return map;
	}
	
	public static char[] readLine() {
		
		String temp = "";
		
		try {
			temp = reader.readLine() + " ";
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		char[] line = temp.toCharArray();
		return line;
	}
	
	public static String[] convertLine(char[] line) {
		
		int count = 0;
		String[] strings;
		String temp = "";
		int i = 0;
		
		for(char c : line) {
			
			if( c == ' ') {
				count++;
			}
		}
		
		strings = new String[count];
		
		for (char c : line) { 
			
			if(c == ' ') {
				strings[i] = temp;
				i++;
				temp = "";
			}else {
				
				temp += c;
				
			}
			
			
		}
		
		
		return strings;
	}
	
	public static int getI() {
		
		return i;
	}
	
	public static int getJ() {
		
		return j;
	}
	

}
