package challenge;

import java.util.ArrayList;

public class MainClass {
	
	private static Map map;
	private static int[][] iMap;
	private static ArrayList<Integer> resultpath = new ArrayList<Integer>();
 
	public static void main(String[] args) {
		
		InputManager.initialize("resources//4x4.txt"); //read text file
		map = new Map(InputManager.getI(),InputManager.getJ()); //create instance of class Map
		iMap = map.getMap(); // get the empty map of the map instance
		map.setMap(InputManager.create(iMap)); //fill the map with content
		iMap = map.getMap(); // update changed map reference
		ExecutionManager.execute(); //run path calculation
		resultpath = ExecutionManager.getPaths().get(0);
		
		for(  ArrayList<Integer> path: ExecutionManager.getPaths()) { // find steepest path
			
			
			if(Rules.calcDrop(path) > Rules.calcDrop(resultpath)) {
				
				resultpath = path;
				
			}
			
			
		}
		
		System.out.println("Steilster und l�ngster Pfad:" + resultpath);
		System.out.println("Steigung:" + Rules.calcDrop(resultpath));
		System.out.println("l�nge:" + Rules.getLength(resultpath));

	}
	
	
	public static int getElement(int i, int j) {
		return iMap[i][j];
	}

}
