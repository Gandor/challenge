package challenge;

import java.util.ArrayList;

public class Rules {
	
	private static ArrayList<Integer> path = new ArrayList<Integer>();
	private static int drop = 0;
	
	
	
	
	public static int[] step(int i1, int j1, int i2, int j2) {
		 int[] step = new int[4];
		if(i1 == i2 && j1 == j2+1) { //step right
			step[0] = i1;
			step[1] = j1;
			step[2] = i2;
			step[3] = j2;
		}else if(i1 == i2 && j1 == j2-1){ // step left
			step[0] = i1;
			step[1] = j1;
			step[2] = i2;
			step[3] = j2;
		}else if(i1 == i2+1 && j1 == j2){ // step north
			step[0] = i1;
			step[1] = j1;
			step[2] = i2;
			step[3] = j2;
		}else if(i1 == i2-1 && j1 == j2){ // step south
			step[0] = i1;
			step[1] = j1;
			step[2] = i2;
			step[3] = j2;
		}else {              // no step
			step[0] = i1;
			step[1] = j1;
			step[2] = i1;
			step[3] = j1;
		}
		
		
		return step;
		
	}
	
	
	public static boolean valid(int[] step) {
		if(MainClass.getElement(step[2],step[3]) > MainClass.getElement(step[0],step[1])) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public static int calcDrop(ArrayList<Integer> path) {
		
		drop = path.get(0) - path.get(path.size()-1);
		
		return drop;
		
	}
		
	
	public static int getLength(ArrayList<Integer> path) {
		
		return path.size();
	}
	
	
	public static ArrayList<Integer> getPath(){
		return path;
	}
	public static void expandPath(int i) {
		path.add(i);
	}
	public static void resetPath(){
		path.clear();
	}

}
