package challenge;

import java.util.ArrayList;

public class ExecutionManager {
	
	private static ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
	private static int maxLength = 0; // saves the length of the actuel longest path 
	private static int oldI = 0;
	private static int oldJ = 0;
	private static int newI = 0;
	private static int newJ = 0;
	private static int step = 0;
	
	public static void execute() {
		
		for(int i = 0;i<InputManager.getI();i++) {
			
			for(int j=0;j< InputManager.getJ();j++) {
				
				backtrack(i,j);
				
			}
			
		}
	}
	
	public static void backtrack(int i,int j) {
		
		 oldI = i;
		 oldJ = j;
		
		
		for (int x = 0;x <4;x++) {
			for (int y = 0;y < 4;y++) {
				
				doStep(x,y);
							
			}
		}

		//guaranties, that only the longest paths are added to the result List
		
		if (Rules.getPath().size() == maxLength ) {
			
			paths.add(Rules.getPath());
			
		}else if(Rules.getPath().size() > maxLength ) {
			paths.clear();
			paths.add(Rules.getPath());
			maxLength = Rules.getPath().size();
		}
		
		
		
		
	}
	
	public static void doStep(int x , int y) {
		
		newI = x;
		newJ = y;

		if(Rules.valid(Rules.step(oldI,oldJ,newI,newJ))) {
			step = MainClass.getElement(newI,newJ);
			Rules.expandPath(step);
			backtrack(newI,newJ);
		}
		
		
	}
	
	public static ArrayList<ArrayList<Integer>> getPaths() {
		
		return paths;
	}
	
	
}
